
import java.io.*;
class Demo15{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Company name : ");
		String cmpName=br.readLine();

		System.out.print("Enter Employee name : ");
		String empName=br.readLine();

		System.out.print("Enter Employee Id : ");
		int empId=Integer.parseInt(br.readLine());

		System.out.print("Enter Employee Salary : ");
		double empSal=Double.parseDouble(br.readLine());
		System.out.println("Company name : "+ cmpName);
		System.out.println("Employee name : "+ empName);
		System.out.println("Employee ID : "+ empId);
		System.out.println("Employee Salary : "+ empSal);
		
	}
}
