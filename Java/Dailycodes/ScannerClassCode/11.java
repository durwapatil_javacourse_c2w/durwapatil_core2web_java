
import java.util.Scanner;
class Demo11{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter name : ");
		String name=sc.next();

		System.out.print("Enter college name : ");
		String clgName=sc.next();

		System.out.print("Enter student ID : ");
		int studId = sc.nextInt();

		System.out.print("Enter CGPA : ");
		float marks=sc.nextFloat();

		System.out.println("Student name : "+ name);
		System.out.println("College name : "+ clgName);
		System.out.println("Student ID : "+ studId);
		System.out.println("CGPA : "+ marks);
	}
}
