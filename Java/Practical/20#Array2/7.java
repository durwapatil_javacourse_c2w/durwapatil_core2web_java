
import java.util.*;
class Demo7{
	public static void main(String [] args){	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size=sc.nextInt();

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Array elements are : ");	
		for(int i=0;i<arr.length;i++){
			if(size%2==1){
				System.out.println(arr[i]);
			}
			else{
				if(i%2==0){
					System.out.println(arr[i]);
				}
			}
		}
	}
}
