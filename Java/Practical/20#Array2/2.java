
import java.util.*;
class Demo2{
	public static void main(String [] arsg){	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size=sc.nextInt();

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int sum=0;
		System.out.print("Elemeents divisible by 3: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%3==0){
				System.out.print(arr[i]+" ");
				sum+=arr[i];
			}
		}
		System.out.println();
		System.out.println("Sum of elements divisible by 3 is : "+sum);
	}
}
