
import java.util.*;
class Demo4{
	public static void main(String [] args){	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size=sc.nextInt();

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter the number to search in array : ");
		int search=sc.nextInt();

		for(int i=0;i<arr.length;i++){
			if(arr[i]==search){
				System.out.println(arr[i]+" is found at index "+i);
			}
		}
	}
}
