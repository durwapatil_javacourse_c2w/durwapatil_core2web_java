
import java.util.*;
class Demo3{
	public static void main(String [] args){	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size=sc.nextInt();

		char arr[]=new char[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);
		}
		
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='A'||arr[i]=='e'||arr[i]=='E'||arr[i]=='i'||arr[i]=='I'||arr[i]=='o'||arr[i]=='O'||arr[i]=='u'||arr[i]=='U'){
				System.out.println("Vowel "+arr[i]+" found at index "+i);
			}
		}
	}
}
