

class Demo9{
	public static void main(String[] args){
		int num=214367689;
		int count1=0;
		int count2=0;
		while(num!=0){
			if(num%2==1){
				count1++;
			}else{
				count2++;
			}
			num/=10;
		}
		System.out.println("Odd count :"+ count1);
		System.out.println("Even count :"+ count2);
	}
}
