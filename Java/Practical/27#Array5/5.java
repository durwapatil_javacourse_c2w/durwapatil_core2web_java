
import java.util.*;
class Demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		
		for(int i=0;i<arr.length;i++){
			int count=0;
			while(arr[i]!=0){
				arr[i]/=10;
				count++;
			}
			System.out.print(count+" ");
		}
		System.out.println();
	}
}
