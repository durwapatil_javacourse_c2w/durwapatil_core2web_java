
import java.util.*;
class Demo1{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the array size : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int count=0;
		int temp=arr[0];
		for(int i=1;i<arr.length;i++){
			if(temp<arr[i]){
				temp=arr[i];
				count++;
			}
		}
		if(count==(arr.length-1)){
			System.out.println("The given array is in ascending order");
		}
		else{
			System.out.println("The given array is not in ascending order");
		}
	}
}
