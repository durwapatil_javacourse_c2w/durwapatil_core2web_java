
import java.util.*;
class Demo7{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		System.out.print("Composite numbers are : ");
		for(int i=0;i<arr.length;i++){
			int count=0;
			int temp=1;
			while(temp<=arr[i]){
				if(arr[i]%temp==0){
					count++;
				}
				temp++;
			}
			if(count>2){
				flag=1;
				System.out.print(arr[i]+" ");
			}
		}
		System.out.println();
		if(flag==0){
			System.out.println("Composite number not found in array");
		}
	}
}
