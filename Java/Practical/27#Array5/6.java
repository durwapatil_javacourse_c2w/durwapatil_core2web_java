
import java.util.*;
class Demo6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size : ");
		int size=sc.nextInt();

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			int temp=1;
			while(temp<=arr[i]){
				if(arr[i]%temp==0){
					count++;
					flag=1;
				}
				temp++;
			}
			if(count==2){
				flag=1;
				System.out.println("First prime numbers found at index "+i);
				break;
			}
			flag=0;
		}
		if(flag==0){
			System.out.println("Prime number is not found");
		}
	}
}
