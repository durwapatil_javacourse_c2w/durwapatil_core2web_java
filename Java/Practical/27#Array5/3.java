import java.util.*;
class Demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Array Size : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		for(int i=0;i<arr.length/2;i++){
			if(arr[i]==arr[size-i-1]){
				flag=1;
			}
			else{
				flag=0;
			}
		}
		if(flag==1){
			System.out.println("The given array is a palindrome array");
		}
		else{
			System.out.println("The given array is not a palindrome array");
		}
	}
}
