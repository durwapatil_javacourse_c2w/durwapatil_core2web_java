
import java.util.*;
class Demo10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<arr.length;i++){
			int num=1;
			int fact=1;
			while(fact<=arr[i]){
				num=num*fact;
				fact++;
			}
			System.out.print(num +" ");
		}
		System.out.println();
	}
}
