
import java.util.*;
class Demo4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter array size : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i]==arr[j]){
					flag=1;
					System.out.println("First duplicate element is present at index : "+i);
					break;	
				}
			}
			if(flag==1){
				break;
			}
		}
		if(flag==0){
			System.out.println("Duplicate element is not present in the array ");
		}
	}
}
