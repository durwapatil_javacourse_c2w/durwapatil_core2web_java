
class program8{
	public static void main(String[] args){
		float percent=89.77f;
		if(percent>=75.00){
			System.out.println("First class with distinction");
		}
		else if(percent<75.00 && percent>=60.00){
			System.out.println("First class");
		}
		else if(percent<60.00 && percent>=50.00){
			System.out.println("Second class");
		}
		else if(percent<50.00 && percent>=35.00){
			System.out.println("Pass");
		}
		else {
			System.out.println("Fail");
		}
	}

}
