
import java.io.*;
class Demo10{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());

		for(int i=row;i>=1;i--){
			int num=i;
			for(int j=1;i>=j;j++){
				if(i%2==1){
					System.out.print((char)(num+96) +" ");
				}else{
					System.out.print((char)(num+64) +" ");
				}
				num--;
			}
			System.out.println();
		}
	}
}
