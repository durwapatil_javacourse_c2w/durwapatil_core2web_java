
import java.io.*;
class Demo1{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=i;
			for(int j=row;i<=j;j--){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}
