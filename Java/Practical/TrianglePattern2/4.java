
import java.io.*;
class Demo4{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		int sum=0;
		for(int i=1;i<=row;i++){
			sum=sum+i;
		}

		for(int i=1;i<=row;i++){
			for(int j=row;i<=j;j--){
				System.out.print((char)(sum+64) +" ");
				sum--;
			}
			System.out.println();
		}
	}
}
