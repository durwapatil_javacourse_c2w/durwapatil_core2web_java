
import java.io.*;
class Demo8{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		int num=row;
		for(int i=1;i<=row;i++){
			int num1=num;
			for(int j=row;i<=j;j--){
				if(i%2==1){
					System.out.print(num1 +" ");
				}else{
					System.out.print((char)(num1+64) +" ");
				}
				num1--;
			}
			num--;
			System.out.println();
		}
	}
}
