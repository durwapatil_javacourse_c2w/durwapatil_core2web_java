
import java.io.*;
class Demo5{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			int num=1;
			for(int j=row;i<=j;j--){
				if(i%2==1){
					System.out.print((char)(num+64) +" ");
				}else{
					System.out.print((char)(num+96) +" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
