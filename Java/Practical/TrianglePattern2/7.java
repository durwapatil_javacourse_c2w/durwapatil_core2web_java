
import java.io.*;
class Demo7{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());

		for(int i=row;i>=1;i--){
			int num=i;
			for(int j=1;i>=j;j++){
				if(j%2==1){
					System.out.print(num +" ");
				}else{
					System.out.print((char)(num+96) +" ");
				}
				num--;
			}
			System.out.println();
		}
	}
}
