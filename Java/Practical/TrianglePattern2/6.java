
import java.io.*;
class Demo6{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());

		for(int i=row;i>=1;i--){
			int num=1;
			char ch='a';
			for(int j=1;i>=j;j++){
				if(j%2==1){
					System.out.print(num +" ");
					num++;
				}else{
					System.out.print(ch +" ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}
