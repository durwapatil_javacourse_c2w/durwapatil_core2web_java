
import java.io.*;
class Demo2{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		int num=2;
		for(int i=1;i<=row;i++){
			for(int j=row;i<=j;j--){
				System.out.print(num +" ");
				num=num+2;
			}
			System.out.println();
		}
	}
}
