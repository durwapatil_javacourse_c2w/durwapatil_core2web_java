
import java.util.*;
class Demo8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==1 && j%2==1){
					int num1=(num*num)-1;
					System.out.print(num1 +"\t");
				}
				else{
					int num1=num+1;
					System.out.print((char)(num1+96) +"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}
