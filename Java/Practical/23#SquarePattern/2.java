
import java.util.*;
class Demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int k=row;k>i;k--){
				System.out.print((char)(num+96)+"\t");
				num++;
			}
			for(int j=1;j<=i;j++){
				System.out.print((char)(num+64)+"\t");
				num++;
			}
			System.out.println();
		}
	}
}
