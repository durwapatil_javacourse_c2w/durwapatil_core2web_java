
import java.util.*;
class Demo9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			for(int k=row;k>i;k--){
				System.out.print(" \t");
			}
			for(int j=1;j<=i*2-1;j++){
				if(i%2==1){
					if(j<i){
						System.out.print((char)(num+64) +"\t");
						num++;
					}
					else{
						System.out.print((char)(num+64)+"\t");
						num--;
					}
				}
				else{
				
					if(j<i){
						System.out.print((char)(num+96) +"\t");
						num++;
					}
					else{
						System.out.print((char)(num+96)+"\t");
						num--;
					}
				}
			}
			System.out.println();
		}
	}
}
