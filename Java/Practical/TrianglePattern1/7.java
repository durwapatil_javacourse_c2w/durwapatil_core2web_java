
import java.io.*;
class Demo7{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;i>=j;j++){
				if(j%2==1){
					System.out.print(i + " ");
				}else{
					System.out.print((char)(num+96)+" ");
					num++;
				}
			}
			System.out.println();
		}
	}
}
