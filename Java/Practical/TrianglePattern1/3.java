
import java.io.*;
class Demo3{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=row;
			for(int j=1;i>=j;j++){
				System.out.print((char)(num+64) + " ");
				num--;
			}
			System.out.println();
		}
	}
}
