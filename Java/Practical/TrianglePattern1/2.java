
import java.io.*;
class Demo2{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			for(int j=1;i>=j;j++){
				if(i%2==1){
					System.out.print((char)(j+96) + " ");
				}else{
					System.out.print("$ ");
				}
			}
			System.out.println();
		}
	}
}
