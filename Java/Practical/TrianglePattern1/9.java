
import java.io.*;
class Demo9{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		int num1=1;
		for(int i=1;i<=row;i++){
			int num2=1+row;
			for(int j=1;i>=j;j++){
				if(j%2==1){
					System.out.print(num2 + " ");
					num2=num2+2;
				}else{
					System.out.print((char)(num1+96)+" ");
					num1++;
				}
			}
			System.out.println();
		}
	}
}
