
import java.io.*;
class Demo6{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;i>=j;j++){
				if(i%2==1){
					System.out.print(j + " ");
				}else if(i%2==0){
					System.out.print((char)(num+64)+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
