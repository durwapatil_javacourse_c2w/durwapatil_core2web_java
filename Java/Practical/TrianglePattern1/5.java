
import java.io.*;
class Demo5{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		int num=row+1;
		for(int i=1;i<=row;i++){
			for(int j=1;i>=j;j++){
				System.out.print((char)(num+64) + " ");
				num++;
			}
			System.out.println();
		}
	}
}
