import java.util.*;
class Demo10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		int num1=row;
		for(int i=1;i<=row;i++){
			int num=65;
			for(int k=1;k<i;k++){
				System.out.print(" \t");
				num++;
			}
			for(int j=row;j>=i;j--){
				if((i+j)%2==0){
					System.out.print((char)num +"\t");
				}
				else{
					System.out.print(num +"\t");
				}
				num++;
			}
			num1--;
			System.out.println();
		}
	}
}
