import java.util.*;
class Demo9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=row +64;
			for(int k=1;k<i;k++){
				System.out.print(" \t");
			}
			for(int j=row;j>=i;j--){
				System.out.print((char)num +"\t");
				num--;
			}
			System.out.println();
		}
	}
}
