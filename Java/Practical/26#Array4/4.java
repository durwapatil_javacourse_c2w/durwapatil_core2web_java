
import java.util.*;
class Demo4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the number to check : ");
		int search=sc.nextInt();
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(search==arr[i]){
				count++;
			}
		}
		if(count>2){
			System.out.println(search+ " occurs more than 2 times in an array ");
		}
		else if(count==2){
			System.out.println(search+ " occurs 2 times in an array ");
		}
		else{
			System.out.println(search +" does not occure more than 2 times in an array ");
		}
	}
}
