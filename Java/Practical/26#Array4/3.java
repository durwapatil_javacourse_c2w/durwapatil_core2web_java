
import java.util.*;
class Demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int max=arr[0];
		for(int i=0;i<arr.length;i++){
			if(max<arr[i]){
				max=arr[i];	
			}
		}
		int max2=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			if(max>arr[i] && max2<arr[i]){
				max2=arr[i];
			}
		}
		System.out.println("The second largest element in the array : "+max2);
	}
}
