
import java.util.*;
class Demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Reversed array : ");
		for(int i=0;i<arr.length/2;i++){
			int temp=arr[i];
			arr[i]=arr[size-i-1];
			arr[size-1-i]=temp;
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+ " ");
		}
		System.out.println();
	}
}
