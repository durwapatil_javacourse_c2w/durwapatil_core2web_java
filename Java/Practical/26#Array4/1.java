
import java.util.*;
class Demo1{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int avg=0;
		int sum=0;
		for(int i=0;i<arr.length;i++){
			sum+=arr[i];
		}
		avg=sum/size;
		System.out.println("Array elements's average is : "+avg);
	}
}
