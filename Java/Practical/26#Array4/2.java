
import java.util.*;
class Demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int maxnum=arr[0];
		int minnum=arr[0];
		for(int i=0;i<arr.length;i++){
			if(maxnum<arr[i]){
				maxnum=arr[i];	
			}
			if(minnum>arr[i]){
				minnum=arr[i];
			}
		}
		System.out.println("The difference between the minimum and maximum element is : "+(maxnum-minnum));
	}
}
