
import java.util.*;
class Demo8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		int col=0;
		int sp=0;
		int num=64;
		for(int i=1;i<row*2;i++){
			if(i<=row){
				sp=row-i;
				col=i*2-1;
				num++;
			}
			else{
				sp=i-row;
				col=col-2;
				num--;
			}
			int num1=num;
			for(int k=1;k<=sp;k++){
				System.out.print("\t");
			}
			for(int j=1;j<=col;j++){
				if(j<=col/2){
					System.out.print((char)(num1--) +"\t");
				}
				else{
					System.out.print((char)(num1++) +"\t");
				}
			}
			System.out.println();
		}
	}
}
