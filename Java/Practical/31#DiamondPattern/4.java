
import java.util.*;
class Demo4{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		int col=0;
		int sp=0;
		for(int i=1;i<row*2;i++){
			int num=1;
			if(i<=row){
				sp=row-i;
				col=i*2-1;
			}
			else{
				sp=i-row;
				col=col-2;
			}
			for(int k=1;k<=sp;k++){
				System.out.print("\t");
			}
			for(int j=1;j<=col;j++){
				if(j<=col/2){
					System.out.print(num++ +"\t");
				}
				else{
					System.out.print(num-- +"\t");
				}
			}
			System.out.println();
		}
	}
}
