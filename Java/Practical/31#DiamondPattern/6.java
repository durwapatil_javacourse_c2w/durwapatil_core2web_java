
import java.util.*;
class Demo6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		int col=0;
		int sp=0;
		int num=0;
		for(int i=1;i<row*2;i++){
			if(i<=row){
				sp=row-i;
				col=i*2-1;
				num++;
			}
			else{
				sp=i-row;
				col=col-2;
				num--;
			}
			for(int k=1;k<=sp;k++){
				System.out.print("\t");
			}
			for(int j=1;j<=col;j++){
				System.out.print(num +"\t");
			}
			System.out.println();
		}
	}
}
