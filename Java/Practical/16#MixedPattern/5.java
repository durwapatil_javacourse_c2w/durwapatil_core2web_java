
import java.util.*;
class Demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;i>=j;j++){
				System.out.print(num*j +" ");
			}
			num++;
			System.out.println();
		}
	}
}
