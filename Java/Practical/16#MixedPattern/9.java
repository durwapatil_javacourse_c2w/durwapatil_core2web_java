
import java.util.*;
class Demo9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			int num2=row-i+1;
			for(int j=row;i<=j;j--){
				if(i%2==1){
					System.out.print(num +" ");
					num++;
				}else{
					System.out.print((char)(num2+64) +" ");
					num2--;
				}
			}
			System.out.println();
		}
	}
}
