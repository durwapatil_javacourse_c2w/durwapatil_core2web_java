
import java.util.*;
class Demo10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number : ");
		long num=sc.nextLong();
		long temp=num;
		long rev=0;
		while(temp>0){
			long rem=temp%10;
			rev=(rev*10)+rem;
			temp/=10;
		}
		while(rev>0){
			long var=rev%10;
			if(var%2==1){
				System.out.print(var*var +" ");
			}
			rev/=10;
		}
		System.out.println();
	}
}
