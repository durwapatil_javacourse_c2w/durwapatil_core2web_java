
import java.util.*;
class Demo10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		int col=0;
		int sp=0;
		int num=row+65;
		for(int i=1;i<row*2;i++){
			if(i<=row){
				col=i;
				sp=row-i;
				num--;
			}
			else{
				col=col-1;
				sp=i-row;
				num++;
			}
			int num1=num;
			for(int k=0;k<sp;k++){
				System.out.print("\t");
			}
			for(int j=1;j<=col;j++){
				System.out.print((char)num1++ +"\t");
			}
			System.out.println();
		}
	}
}
