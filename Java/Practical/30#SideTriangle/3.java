
import java.util.*;
class Demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		int col=0;
		int num=0;
		for(int i=1;i<row*2;i++){
			if(i<=row){
				col=i;
				num++;
			}
			else{
				col=col-1;
				num--;
			}
			int num1=num;
			for(int j=1;j<=col;j++){
				System.out.print(num1 +"\t");
				num1--;
			}
			System.out.println();
		}
	}
}
