
import java.util.*;
class Demo8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number : ");
		int num=sc.nextInt();
		int temp=0;
		while(num!=0){
			temp=num%10;
			if(temp==0){
				System.out.println("It is a Duck number ");
				break;
			}
			num/=10;
		}
		if(num==0){
			System.out.println("It is not a duck number");
		}
	}
}
