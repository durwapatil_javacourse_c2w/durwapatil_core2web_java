
import java.util.*;
class Demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number : ");
		int num=sc.nextInt();
		int sum=0;
		int num1=num;
		int temp=0;
		while(num1!=0){
			temp=num1%10;
			int mul=1;
			while(temp>=1){
				mul=mul*temp;
				temp--;
			}
			sum=sum+mul;
			num1/=10;
		}
		if(sum==num){
			System.out.println("It is a strong number");
		}
		else{
			System.out.println("It is not a strong number ");
		}
	}
}
