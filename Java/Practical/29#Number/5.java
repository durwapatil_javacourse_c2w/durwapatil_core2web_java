
import java.util.*;
class Demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number : ");
		int num=sc.nextInt();
		int count=0;
		int num1=num;
		while(num!=0){
			num/=10;
			count++;
		}
		int sq=num1*num1;
		int var=0,rem=0,rev=0;
		while(count!=0){
			rem=sq%10;
			var=rem+var*10;
			sq/=10;
			count--;
		}
		while(var!=0){
			rem=var%10;
			rev=rem+rev*10;
			var/=10;
		}
		if(rev==num1){
			System.out.println("It is a automorphic number");
		}
		else{
			System.out.println("It is not a automorphic number ");
		}
	}
}
