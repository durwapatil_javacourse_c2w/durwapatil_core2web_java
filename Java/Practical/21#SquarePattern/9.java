
import java.util.*;
class Demo9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if((i+j)%2==0){
					System.out.print((j*2)+"\t");
				}
				else{
					System.out.print((j*3) +"\t");
				}
			}
			System.out.println();
		}
	}
}
