
import java.util.*;
class Demo10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=row;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					if(j%2==1){
						System.out.print(num +"\t");
					}
					else{
						System.out.print((char)(num+64) +"\t");
					}
				}
				else{
					System.out.print((char)(num+64)+"\t");
				}
				num--;
			}
			System.out.println();
		}
	}
}
