
import java.util.*;
class Demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner (System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		int num=row;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(num%3==0){
					System.out.print(num*3 +"\t");
				}
				else if(num%5==0){
					System.out.print(num*5 +"\t");
				}
				else{
					System.out.print(num +"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}
