
import java.util.*;
class Demo1{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number of rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=row+64;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print((char)num +"\t");
					num--;
				}
				else{
					System.out.print(row +"\t");
				}
			}
			System.out.println();
		}
	}
}
