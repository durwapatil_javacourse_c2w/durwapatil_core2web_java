
import java.util.*;
class Demo6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of array : ");
		int size=sc.nextInt();
		char arr[]=new char[size];

		for(int i=0;i<size;i++){
			arr[i]=sc.next().charAt(0);
		}
		System.out.print("Elements are : ");
		for(int i=0;i<size;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
