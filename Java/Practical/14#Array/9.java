
import java.util.*;
class Demo9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter elements : ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		
		for(int i=0;i<size;i++){
			if(i%2==1){
				System.out.println(arr[i]+" is an odd indexed element ");
			}
		}
	}
}
