class Demo10{
	public static void main(String[] args){
		System.out.println("For loop starts");
		for(int i=1;i<=10;i++){
			if(i%2==0){
				System.out.println(i +" is the number which is divisible by 2");
			}
			else{
				System.out.println(i +" is the numbers which is not divisible by 2");
			}
		}
		System.out.println("For loop ends");
		System.out.println("While loop starts");
		int i=1;
		while(i<=10){
			if(i%2==0){
				System.out.println(i +" is the number which is divisible by 2");
			}
			else{
				System.out.println(i +" is the numbers which is not divisible by 2");
			}
			i++;
		}
		System.out.println("While loop ends");
	}
}
