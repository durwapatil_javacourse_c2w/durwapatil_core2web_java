
import java.util.*;
class Demo1{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number : ");
		int num=sc.nextInt();
		int temp=1;
		System.out.print("Factors of "+num +" are ");
		while(temp<=num){
			if(num%temp==0){
				System.out.print(temp+", ");
			}
			temp++;
		}
		System.out.println();
	}
}
