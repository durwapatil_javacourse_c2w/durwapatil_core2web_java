
import java.util.*;
class Demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number : ");
		int num=sc.nextInt();
		int temp=1;
		int count=0;
		while(temp<=num){
			if(num%temp==0){
				count++;
			}
			temp++;
		}
		if(count==2){
			System.out.println(num +" is not a composite number");
		}else{
			System.out.println(num +" is a composite number");
		}
	}
}
