
import java.util.*;
class Demo7{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number : ");
		int num=sc.nextInt();
		int fact=1;
		int i=1;
		while(i<=num){
			fact=fact*i;
			i++;
		}
		System.out.println("Factorial of "+num+" is "+fact);
	}
}
