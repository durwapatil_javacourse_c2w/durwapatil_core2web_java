
import java.util.*;
class Demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array : ");
		int size=sc.nextInt();

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int count=0;
		System.out.print("Enter the element to be searched : ");
		int search=sc.nextInt();
		for(int i=0;i<arr.length;i++){
			if(arr[i]==search){
				System.out.print("num "+search+" first occured at index : "+i);
				count++;
				break;
			}
		}
		if(count!=1){
			System.out.println("\nnum "+search+" not found in the array");
		}
		System.out.println();
	}
}
