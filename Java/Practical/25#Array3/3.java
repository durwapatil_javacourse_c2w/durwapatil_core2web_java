
import java.util.*;
class Demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array : ");
		int size=sc.nextInt();

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int count=0;
		System.out.print("Enter the element to be searched : ");
		int search=sc.nextInt();
		for(int i=0;i<arr.length;i++){
			if(arr[i]==search){
				count++;
			}
		}
		if(count==0){
			System.out.println("Number "+search+" not found in the array");
		}
		else{
			System.out.println("Number "+search+" occurred "+count+" times in an array");
		}
	}
}
