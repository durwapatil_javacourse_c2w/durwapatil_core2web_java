
import java.util.*;
class Demo1{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int num1=arr[0];
		int count=0;
		for(int i=1;i<arr.length;i++){
			if(num1>arr[i]){
				count++;
				num1=arr[i];
			}
		}
		if(count==(arr.length-1)){
			System.out.println("Given array is in descending order");
		}
		else{
			System.out.println("Given array is not in descending order");
		}
	}
}
