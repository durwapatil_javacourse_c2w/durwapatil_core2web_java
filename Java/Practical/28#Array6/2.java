
import java.util.*;
class Demo2{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int sum=0;
		int count1=0,temp=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			temp=1;
			while(temp<=arr[i]){
				if(arr[i]%temp==0){
					count++;
				}
				temp++;
			}
			if(count==2){
				sum=sum+arr[i];
				count1++;
			}
		}
		System.out.println("Sum of prime numbers : "+sum);
		System.out.println("Count of prime numbers : "+count1);
	}
}
