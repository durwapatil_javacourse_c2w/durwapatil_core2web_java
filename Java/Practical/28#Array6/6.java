
import java.util.*;
class Demo6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter key : ");
		int key=sc.nextInt();
		int flag=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%key==0){
				System.out.println("An element multiple of 5 found at index : "+i);
				flag=1;
			}
		}
		if(flag==0){
			System.out.println("Element not found");
		}
	}
}
