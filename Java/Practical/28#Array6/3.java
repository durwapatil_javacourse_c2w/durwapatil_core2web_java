
import java.util.*;
class Demo3{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int count=0;
		System.out.print("Enter key : ");
		int key=sc.nextInt();
		for(int i=0;i<arr.length;i++){
			if(key==arr[i]){
				count++;
			}
		}
		if(count>2){
			for(int i=0;i<arr.length;i++){
				if(key==arr[i]){
					System.out.print((key*key*key)+" ");
				}
				else{
					System.out.print(arr[i] +" ");
				}
			}
		}
		else{
			System.out.println("Element not found");
		}
	}
}
