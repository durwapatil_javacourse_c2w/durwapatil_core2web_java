
import java.util.*;
class Demo8{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		char arr[]=new char[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next().charAt(0);
		}
		
		System.out.print("Before Reverse : ");
		for(int i=0;i<arr.length;i++){
			if(i%2==0){
				System.out.print(arr[i] +" ");
			}	
		}
		System.out.println();
		char temp=' ';
		for(int i=0;i<size/2;i++){
			temp=arr[i];
			arr[i]=arr[size-i-1];
			arr[size-i-1]=temp;
		}
		
		System.out.print("After Reverse : ");
		for(int i=0;i<arr.length;i++){
			if(i%2==0){
				System.out.print(arr[i] +" ");
			}	
		}
		System.out.println();

		
	}
}
