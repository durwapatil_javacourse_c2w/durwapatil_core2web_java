
import java.util.*;
class Demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of first array : ");
		int size1=sc.nextInt();
		int arr1[]=new int[size1];
		System.out.println("Enter elements of first array : ");
		for(int i=0;i<size1;i++){
			arr1[i]=sc.nextInt();
		}
		System.out.print("Enter size of second array : ");
		int size2=sc.nextInt();
		int arr2[]=new int[size2];
		System.out.println("Enter elements of second array : ");
		for(int i=0;i<size2;i++){
			arr2[i]=sc.nextInt();
		}
		int size3= size1+size2;
		int arr3[]=new int[size3];
		int index=0;
		for(int i=0;i<size3;i++){
			if(i<size1){
				arr3[i]=arr1[i];
			}
			else{
				arr3[i]=arr2[index];
				index++;
			}
			System.out.print(arr3[i]+ " ");

		}
		System.out.println();
	}
}
