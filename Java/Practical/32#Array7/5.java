
import java.util.*;
class Demo5{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		System.out.print("Enter number of columns : ");
		int col=sc.nextInt();
		int arr[][]=new int[row][col];
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=sc.nextInt();
			}
		}
		int temp=0;
		for(int i=0;i<row;i++){
			int sum=0;
			for(int j=0;j<col;j++){
				temp=arr[j][i];
				sum=sum+temp;
			}
			System.out.println("Sum of row "+(i+1)+" = "+sum);
		}
	}
}
