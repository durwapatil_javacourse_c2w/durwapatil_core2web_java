
import java.util.*;
class Demo7{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		System.out.print("Enter number of columns : ");
		int col=sc.nextInt();
		int arr[][]=new int[row][col];
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=sc.nextInt();
			}
		}
		int prod=1;
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				if(i==j){
					prod=prod*arr[i][j];
				}
			}
		}
		System.out.println("Product of Primary Diagonal : "+prod);
	}
}
