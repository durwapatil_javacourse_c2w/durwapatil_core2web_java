
import java.util.*;
class Demo9{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
		System.out.print("Enter number of columns : ");
		int col=sc.nextInt();
		int arr[][]=new int[row][col];
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=sc.nextInt();
			}
		}
		int sum1=0;
		int sum2=0;
		int prod=1;
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				if((i+j)==(row-1)){
					sum1=sum1+arr[i][j];
				}
				if(i==j){
					sum2=sum2+arr[i][j];
				}
			}
		}
		prod=sum1*sum2;
		System.out.println("Product of Sum of Primary and Secondary Diagonal : "+prod);
	}
}
