
import java.io.*;
class Demo9{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){
				System.out.print((char)(j+64) +" ");
			}
			System.out.println();
		}
	}
}
