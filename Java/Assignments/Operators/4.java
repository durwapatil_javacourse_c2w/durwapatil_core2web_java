
class Demo4{
	public static void main(String[] args){
		boolean b1=true;
		boolean b2=false;

		System.out.println("Logical AND :"+  (b1&&b2));
		System.out.println("Logical OR :"+  (b1||b2));
		System.out.println("Logical NOT for the first value :"+  (!b1));
		System.out.println("Logical NOT for the second value :"+  (!b2));
	}
}
