class Demo5{
	public static void main(String[] args){
		int num1=5;
		int num2=3;
		
		System.out.println("Bitwise AND:"+ (num1&num2));
		System.out.println("Bitwise OR:"+ (num1|num2));
		System.out.println("Bitwise XOR:"+ (num1^num2));
		System.out.println("Left Shift:"+ (num1<<1));
		System.out.println("Right Shift:"+ (num1>>1));
	
	}
}
