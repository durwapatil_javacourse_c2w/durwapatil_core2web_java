class IfDemo2{
	public static void main(String[] args){
		char ch='O';
		if(ch=='O'){
			System.out.println("Outstanding");
		}else if(ch=='A') {
			System.out.println("Excellent");
		}else if(ch=='B'){
			System.out.println("Good");
		}else if(ch=='C'){
			System.out.println("Nice try");
		}else if (ch=='D'){
			System.out.println("Pass");
		}else{
			System.out.println("Fail");
		}
	}
}
