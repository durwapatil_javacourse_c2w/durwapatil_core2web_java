class IfDemo3{
	public static void main(String[] args){
		String st= "S";
		if(st=="XXS"){
			System.out.println("Extra Extra Small");
		}else if(st=="XS") {
			System.out.println("Extra Small");
		}else if(st=="S"){
			System.out.println("Small");
		}else if(st=="L"){
			System.out.println("Large");
		}else if (st=="XL"){
			System.out.println("Extra Large");
		}else{
			System.out.println("Extra Extra Large");
		}
	}
}
