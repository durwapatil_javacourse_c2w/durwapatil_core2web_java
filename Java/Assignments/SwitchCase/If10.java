

class IfDemo10{
	public static void main(String[] args){
		int criteria =5;
		if(criteria==1){
			System.out.println("Candidate has knowledge of Java Language");
		}else if(criteria==2){
			System.out.println("Candidate has knowledge of Java Language and OOPS");
		}else if(criteria==3){
			System.out.println("Candidate has knowledge of Java Language , OOPS and DSA");
		}else if(criteria==4){
			System.out.println("Candidate has knowledge of Java Language, OOPS ,DSA and DBMS");
		}else if(criteria==5){
			System.out.println("Candidate has knowledge of Java Language, OOPS ,DSA ,DBMS and Web Development");
		}else{
			System.out.println("Criteria not satisfied");
		}
	}
}
