class SwitchDemo2{
	public static void main(String[] args){
		char ch='O';
		switch(ch){
			case 'O':
				System.out.println("Outstanding");
				break;
			case 'A':
				System.out.println("Excellent");
				break;
			case 'B':
				System.out.println("Good");
				break;
			case 'C':
				System.out.println("Nice try");
				break;
			case 'D':
				System.out.println("Pass");
				break;
			case 'P':
				System.out.println("Fail");
				break;
		}
	}
}
