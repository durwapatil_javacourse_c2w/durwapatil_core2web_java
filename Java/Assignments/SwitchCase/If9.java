

class IfDemo9{
	public static void main(String[] args){
		int eng=87;
		int hindi=85;
		int maths=94;
		int science=24;
		int sanskrit=95;
		if(eng>=35 && hindi>=35 && maths>=35 && science>=35 && sanskrit>=35){
			int grade=(eng + hindi + maths + science + sanskrit)/5;
			if(grade>=90){
				System.out.println("First class with destination");
			}else if(grade>=80){
				System.out.println("First class");
			}else if(grade>=70){
				System.out.println("Second class");
			}
		}else{
			System.out.println("You failed the exam");
		}
	}
}
