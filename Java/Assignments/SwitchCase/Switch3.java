class SwitchDemo3{
	public static void main(String[] args){
		String st= "S";
		switch(st){
			case "XXS":
				System.out.println("Extra Extra Small");
				break;
			case "XS": 
				System.out.println("Extra Small");
				break;
			case "S":
				System.out.println("Small");
				break;
			case "L":
				System.out.println("Large");
				break;
			case "XL":
				System.out.println("Extra Large");
				break;
			case "XXL":
				System.out.println("Extra Extra Large");
				break;
			
		}
	}
}
