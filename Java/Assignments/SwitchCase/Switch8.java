

class SwitchDemo8{
	public static void main(String[] args){
		int num1=10;
		int num2=5;
		int num3=num1*num2;
		if(num3>0){
			switch(num3%2){
				case 0:
					System.out.println("Number is even");
					break;
				case 1:
					System.out.println("Number is odd");
					break;
			}
		}else{
			System.out.println("Sorry negative numbers not allowed");
		}
	
	}
}
