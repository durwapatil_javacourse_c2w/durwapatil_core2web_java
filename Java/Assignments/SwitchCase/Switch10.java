

class SwitchDemo10{
	public static void main(String[] args){
		int criteria =5;
		switch(criteria){
			case 1:
				System.out.println("Candidate has knowledge of Java Language");
				break;
			case 2:
				System.out.println("Candidate has knowledge of Java Language and OOPS");
				break;
			case 3:
				System.out.println("Candidate has knowledge of Java Language , OOPS and DSA");
				break;
			case 4:
				System.out.println("Candidate has knowledge of Java Language, OOPS ,DSA and DBMS");
				break;
			case 5:
				System.out.println("Candidate has knowledge of Java Language, OOPS ,DSA ,DBMS and Web Development");
				break;
			default:
				System.out.println("Criteria not satisfied");
		
		}
	}
}
