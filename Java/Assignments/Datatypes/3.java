

class Demo3{
	public static void main(String[] args){
		float pi=3.14f;
		float radius=5.0f;
		int highangle=360;
		float area= pi*radius*radius;
		System.out.println("Area of circle : "+ area);
		System.out.println("Highest degree of circle : "+ highangle);
	
	}
}
