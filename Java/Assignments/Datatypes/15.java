
class Demo15{
	public static void main(String[] args){
		int studId=101;
		String studName="Alex";
		float studMarks=89.6f;
		boolean studAttendance=true;
		char studRank='A';

		System.out.println("Student Id : "+studId);
		System.out.println("Student Name : "+studName);
		System.out.println("Student Marks : "+studMarks);
		System.out.println("Student Attendance : "+studAttendance);
		System.out.println("Student Rank : "+studRank);
	}
}
