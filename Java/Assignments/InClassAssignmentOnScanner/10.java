
import java.util.*;
class Demo10{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in); 

		System.out.print("Enter marks of Mathematics : ");
		int math=sc.nextInt();

		System.out.print("Enter marks of English : ");
		int eng=sc.nextInt();

		System.out.print("Enter marks of Science : ");
		int sci=sc.nextInt();

		System.out.print("Enter marks of Marathi : ");
		int marathi=sc.nextInt();
		
		if(math>0 && math<=100 && eng>0 && eng<=100 && sci>0 && sci<=100 && marathi>0 && marathi<=100){
			int sum= math + eng + sci + marathi;
			System.out.println(sum +" out of 400");
		}else{
			System.out.println("Invalid marks");
		}
	}
}
